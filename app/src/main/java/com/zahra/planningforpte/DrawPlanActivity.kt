package com.zahra.planningforpte

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.zahra.planningforpte.R
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.activity_draw_plan.*
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener


class DrawPlanActivity : AppCompatActivity() {

    private var _xDelta: Int = 0
    private var _yDelta: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_draw_plan)
        val layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(150, 150)
        test_btn.setLayoutParams(layoutParams)
        test_btn.setOnTouchListener(ChoiceTouchListener())
    }

    private inner class ChoiceTouchListener : OnTouchListener {
        override fun onTouch(view: View, event: MotionEvent): Boolean {
            val X = event.rawX.toInt()
            val Y = event.rawY.toInt()
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    val lParams: RelativeLayout.LayoutParams = view.getLayoutParams() as RelativeLayout.LayoutParams
                    _xDelta = X - lParams.leftMargin
                    _yDelta = Y - lParams.topMargin
                }
                MotionEvent.ACTION_UP -> {
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                }
                MotionEvent.ACTION_POINTER_UP -> {
                }
                MotionEvent.ACTION_MOVE -> {
                    val layoutParams: RelativeLayout.LayoutParams= view
                            .getLayoutParams() as RelativeLayout.LayoutParams
                    layoutParams.leftMargin = X - _xDelta
                    layoutParams.topMargin = Y - _yDelta
                    layoutParams.rightMargin = -250
                    layoutParams.bottomMargin = -250
                    view.setLayoutParams(layoutParams)
                }
            }
            root_view.invalidate()
            return true
        }
    }
}
